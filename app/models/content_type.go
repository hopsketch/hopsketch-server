package models

import (
    "labix.org/v2/mgo"
    "labix.org/v2/mgo/bson"
    "github.com/revel/revel"
)

type (
    Field struct {
        Model                                `bson:",inline"`
        Type               string            `bson:"type"`
        Label              string            `bson:"label"`
        Description        string            `bson:"description"`
    }
    ContentType struct {
        Model                                `bson:",inline"`
        Name               string            `bson:"name"`
        Description        string            `bson:"description"`
        Ref_displays       []bson.ObjectId   `bson:"ref_displays"`
        Ref_project        bson.ObjectId     `bson:"ref_project"`
        Fields             []Field           `bson:"fields"`
    }
)

/**
 * Get a content type by id.
 */
func GetContentTypeByObjectId(s *mgo.Session, Id bson.ObjectId) *ContentType {
    ct := new(ContentType)
    Collection(ct, s).FindId(Id).One(ct)
    return ct
}

/**
 * Get all content types.
 */
func GetAllContentTypes(s *mgo.Session) []*ContentType {
    content_types := []*ContentType{}
    ct := new(ContentType)
    Collection(ct, s).Find(nil).All(&content_types)
    return content_types
}

/**
 * Create a content type instance.
 */
func (ct *ContentType) Save(s *mgo.Session) error {
    coll := Collection(ct, s)
    _, err := coll.Upsert(bson.M{"_id": ct.Id}, ct)
    if err != nil {
        revel.WARN.Printf("Unable to save content type: %v error %v", ct, err)
    }
    return err
}

/**
 * Delete the given content type instance by the given id.
 */
func (ct *ContentType) Delete(s *mgo.Session) error {
    coll := Collection(ct, s)
    err := coll.RemoveId(ct.Id)
    if err != nil {
        revel.WARN.Printf("Undable to delete content type: %v error %v", ct, err)
    }
    return err
}