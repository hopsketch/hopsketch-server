package models

import (
    "labix.org/v2/mgo"
    "labix.org/v2/mgo/bson"
    "github.com/revel/revel"
)

type Page struct {
    Model                                  `bson:",inline"`
    Title                string            `bson:"title"`
    Description          string            `bson:"description"`
    Ref_project          bson.ObjectId     `bson:"ref_project"`
    Ref_content_blocks   []bson.ObjectId   `bson:"ref_content_blocks"`
}

/**
 * Get a page by id.
 */
func GetPageByObjectId(s *mgo.Session, Id bson.ObjectId) *Page {
    p := new(Page)
    Collection(p, s).FindId(Id).One(p)
    return p
}

/**
 * Get all pages.
 */
func GetAllPages(s *mgo.Session) []*Page {
    pages := []*Page{}
    p := new(Page)
    Collection(p, s).Find(nil).All(&pages)
    return pages
}

/**
 * Create a page instance.
 */
func (p *Page) Save(s *mgo.Session) error {
    coll := Collection(p, s)
    _, err := coll.Upsert(bson.M{"_id": p.Id}, p)
    if err != nil {
        revel.WARN.Printf("Unable to save page: %v error %v", p, err)
    }
    return err
}

/**
 * Delete the given page instance by the given id.
 */
func (p *Page) Delete(s *mgo.Session) error {
    coll := Collection(p, s)
    err := coll.RemoveId(p.Id)
    if err != nil {
        revel.WARN.Printf("Undable to delete page: %v error %v", p, err)
    }
    return err
}