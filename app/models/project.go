package models

import (
    "labix.org/v2/mgo"
    "labix.org/v2/mgo/bson"
    "github.com/revel/revel"
)

type Project struct {
    Model                                 `bson:",inline"`
    Name                string            `bson:"name"`
    Description         string            `bson:"description"`
    Ref_pages           []bson.ObjectId   `bson:"ref_pages"`
    Ref_content_types   []bson.ObjectId   `bson:"ref_content_types"`
}

/**
 * Get a project by id.
 */
func GetProjectByObjectId(s *mgo.Session, Id bson.ObjectId) *Project {
    p := new(Project)
    Collection(p, s).FindId(Id).One(p)
    return p
}

/**
 * Get all projects.
 */
func GetAllProjects(s *mgo.Session) []*Project {
    projects := []*Project{}
    p := new(Project)
    Collection(p, s).Find(nil).All(&projects)
    return projects
}

/**
 * Create a project instance.
 */
func (p *Project) Save(s *mgo.Session) error {
    coll := Collection(p, s)
    _, err := coll.Upsert(bson.M{"_id": p.Id}, p)
    if err != nil {
        revel.WARN.Printf("Unable to save project: %v error %v", p, err)
    }
    return err
}

/**
 * Delete the given project instance by the given id.
 */
func (p *Project) Delete(s *mgo.Session) error {
    coll := Collection(p, s)
    err := coll.RemoveId(p.Id)
    if err != nil {
        revel.WARN.Printf("Undable to delete project: %v error %v", p, err)
    }
    return err
}

/*func (p *Project) Validate(v *revel.Validation) {
	v.Check(p.Title,
		revel.Required{},
		revel.MinSize{1},
		revel.MaxSize{256},
	)
	v.Check(p.Description,
		revel.Required{},
		revel.MinSize{1},
		revel.MaxSize{1024},
	)
}*/
