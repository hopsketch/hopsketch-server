package models

import (
    "labix.org/v2/mgo"
    "labix.org/v2/mgo/bson"
    "github.com/revel/revel"
)

type (
    Setting struct {
        NumberOfElements   string             `bson:"number_of_elements"`
        NumberOfRows       string             `bson:"number_of_rows"`
        ItemsPerRow        string             `bson:"items_per_row"`
        Size               []int32            `bson:"size"`
        Position           []int32            `bson:"position"`
    }
    ContentBlock struct {
        Model                                 `bson:",inline"`
        Title               string            `bson:"title"`
        Description         string            `bson:"description"`
        Ref_pages           []bson.ObjectId   `bson:"ref_pages"`
        Ref_display         []bson.ObjectId   `bson:"ref_display"`
        Settings            Setting           `bson:"settings"`
    }
)

/**
 * Get a content block by id.
 */
func GetContentBlockByObjectId(s *mgo.Session, Id bson.ObjectId) *ContentBlock {
    c := new(ContentBlock)
    Collection(c, s).FindId(Id).One(c)
    return c
}

/**
 * Get all content blocks.
 */
func GetAllContentBlocks(s *mgo.Session) []*ContentBlock {
    content_blocks := []*ContentBlock{}
    c := new(ContentBlock)
    Collection(c, s).Find(nil).All(&content_blocks)
    return content_blocks
}

/**
 * Create a content block instance.
 */
func (c *ContentBlock) Save(s *mgo.Session) error {
    coll := Collection(c, s)
    _, err := coll.Upsert(bson.M{"_id": c.Id}, c)
    if err != nil {
        revel.WARN.Printf("Unable to save content block: %v error %v", c, err)
    }
    return err
}

/**
 * Delete the given content block instance by the given id.
 */
func (c *ContentBlock) Delete(s *mgo.Session) error {
    coll := Collection(c, s)
    err := coll.RemoveId(c.Id)
    if err != nil {
        revel.WARN.Printf("Undable to delete content block: %v error %v", c, err)
    }
    return err
}
