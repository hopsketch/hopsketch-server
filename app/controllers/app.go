package controllers

import (
	"github.com/jgraham909/revmgo"
	"github.com/revel/revel"
)

type App struct {
	*revel.Controller
	revmgo.MongoController
}

// Responsible for doing any necessary setup for each web request.
func (c *App) Setup() revel.Result {
	// If there is an active user session load the User data for this user.
	return nil
}

func (c App) Index() revel.Result {
	return c.Render()
}
