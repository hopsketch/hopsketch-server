package controllers

import (
    "github.com/revel/revel"
    models "bitbucket.org/hopsketch/hopsketch-server/app/models"
    "labix.org/v2/mgo/bson"
    "encoding/json"
)

type ContentBlock struct {
    App
}

/**
 * List all content blocks.
 */
func (c ContentBlock) List() revel.Result {
    content_blocks := models.GetAllContentBlocks(c.MongoSession)
    c.Response.Out.Header().Set("Access-Control-Allow-Origin", "*")
    return c.RenderJson(content_blocks)
}

/**
 * Get a specific content block by id.
 */
func (c ContentBlock) Get(id bson.ObjectId) revel.Result {
    if id.Hex() != "" {
        content_block := models.GetContentBlockByObjectId(c.MongoSession, id)
        c.Response.Out.Header().Set("Access-Control-Allow-Origin", "*")
        return c.RenderJson(content_block)
    }
    return c.NotFound("Invalid content block Id.")
}

/**
 * Delete a specific content block by id.
 */
func (c ContentBlock) Delete(id bson.ObjectId) revel.Result {
    c.Response.Out.Header().Set("Access-Control-Allow-Origin", "*")
    content_block := models.GetContentBlockByObjectId(c.MongoSession, id)
    content_block.Delete(c.MongoSession)
    return c.RenderJson(content_block)
}

/**
 * Create a content block.
 */
func (c ContentBlock) Create() revel.Result {
    c.Response.Out.Header().Set("Access-Control-Allow-Origin", "*")

    content_block, err := c.parseContentBlock()
    if err != nil {
        return c.RenderText("Unable to parse the Content Block.")
    }
    content_block.Id = bson.NewObjectId()
    content_block.Save(c.MongoSession)
    return c.RenderJson(content_block)
}

/**
 * Update a specific content block by id.
 */
func (c ContentBlock) Update(id string) revel.Result {
    c.Response.Out.Header().Set("Access-Control-Allow-Origin", "*")

    content_block, err := c.parseContentBlock()
    if err != nil {
        return c.RenderText("Unable to parse the Content Block.")
    }
    content_block.Id = bson.ObjectIdHex(id)
    content_block.Save(c.MongoSession)
    return c.RenderJson(content_block)
}

/**
 * Manage CROS preflight requests.
 */
func (c ContentBlock) Preflight() revel.Result {
    c.Response.Out.Header().Set("Access-Control-Allow-Origin", "*")
    c.Response.Out.Header().Set("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS")
    c.Response.Out.Header().Set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    return c.RenderJson("ok")
}

/**
 * Parse a JSON Content Block model object from the request body.
 */
func (c ContentBlock) parseContentBlock() (models.ContentBlock, error) {
    content_block := models.ContentBlock{}
    err := json.NewDecoder(c.Request.Body).Decode(&content_block)
    return content_block, err
}
