conn = new Mongo('127.0.0.1:27017');
db = db.getSiblingDB('hopsketch');
db.dropDatabase();

// create database and collections
db = db.getSiblingDB('hopsketch');
db.createCollection('projects');
db.createCollection('pages');
db.createCollection('content_blocks');
db.createCollection('content_types');
db.createCollection('displays');
//db.createCollection('menus');

/************
 * PROJECTS *
 ************/

// example empty project
var empty = {
  name : 'Empty',
  description : 'It is an empty project.',
  ref_pages : [],
  ref_content_types : []
} 
// example project: index
var index = {
  name : 'Index',
  description : 'It is an example project, which represents the hungarian news portal, index.hu.',
  ref_pages : [],
  ref_content_types : []
}

// insert project to the database
db.projects.insert(empty);
db.projects.insert(index);
var indexId = db.projects.find(index).next()._id;


/*********
 * PAGES *
 *********/
var main_page = {
  title : 'Main Page',
  description : 'The initial or main web page of the index.hu site.',
  ref_project : '',
  ref_content_blocks : []
}

var sport_news_page = {
  title : 'Sport News',
  description : 'This page provides news about world wide sport events.',
  ref_project : '',
  ref_content_blocks : []
}

// inster pages to the database
db.pages.insert(main_page);
var mainPageId = db.pages.find(main_page).next()._id;
db.projects.update({_id : indexId}, {$push: {ref_pages : mainPageId}});
db.pages.update({_id : mainPageId}, {$set : {ref_project : indexId}});

db.pages.insert(sport_news_page);
var sportNewsPageId = db.pages.find(sport_news_page).next()._id;
db.projects.update({_id : indexId}, {$push: {ref_pages : sportNewsPageId}});
db.pages.update({_id : sportNewsPageId}, {$set : {ref_project : indexId}});

/******************
 * CONTENT BLOCKS *
 ******************/
// create content_block variables
var latest_news = {
  title : 'Latest News',
  description : 'This block contains the latest news from the world.',
  ref_pages : [],
  ref_display : '',
  settings : {
    number_of_elements: '10',
    number_of_rows: '10',
    items_per_row: '1',
    size: [4, 6],
    position: [1, 1]
  }
}

var most_readed_news = {
  title : 'Most Readed News',
  description : 'This block contains the most readed news on the site.',
  ref_pages : [],
  ref_display : '',
  settings : {
    number_of_elements: '5',
    number_of_rows: '5',
    items_per_row: '1',
    size: [2, 4],
    position: [5, 5]
  }
}

var sport_news = {
  title : 'Sport News',
  description : 'This block contains the latest sport news from the world.',
  ref_pages : [],
  ref_display : '',
  settings : {
    number_of_elements: '5',
    number_of_rows: '5',
    items_per_row: '1',
    size: [2, 4],
    position: [10, 10]
  }
}

var videos = {
  title : 'Videos',
  description : 'This block contains famous or interesting videos.',
  ref_pages : [],
  ref_display : '',
  settings : {
    number_of_elements: '5',
    number_of_rows: '1',
    items_per_row: '5',
    size: [2, 4],
    position: [15, 10]
  }
}

var photos = {
  title : 'Photos',
  description : 'This block contains pohoto galeries.',
  ref_pages : [],
  ref_display : '',
  settings : {
    number_of_elements: '10',
    number_of_rows: '2',
    items_per_row: '5',
    size: [2, 4],
    position: [20, 20]
  }
}

// insert content_blocks to the database
db.content_blocks.insert(latest_news);
var latestNewsBlockId = db.content_blocks.find(latest_news).next()._id;
db.pages.update({_id : mainPageId}, {$push: {ref_content_blocks : latestNewsBlockId}});
db.content_blocks.update({_id : latestNewsBlockId}, {$push: {ref_pages : mainPageId}});

db.content_blocks.insert(most_readed_news);
var mostReadedNewsBlockId = db.content_blocks.find(most_readed_news).next()._id;
db.pages.update({_id : mainPageId}, {$push: {ref_content_blocks : mostReadedNewsBlockId}});
db.content_blocks.update({_id : mostReadedNewsBlockId}, {$push: {ref_pages : mainPageId}});

db.content_blocks.insert(sport_news);
var sportNewsBlockId = db.content_blocks.find(sport_news).next()._id;
db.pages.update({_id : mainPageId}, {$push: {ref_content_blocks : sportNewsBlockId}});
db.content_blocks.update({_id : sportNewsBlockId}, {$push: {ref_pages : mainPageId}});

db.content_blocks.insert(videos);
var videosBlockId = db.content_blocks.find(videos).next()._id;
db.pages.update({_id : mainPageId}, {$push: {ref_content_blocks : videosBlockId}});
db.content_blocks.update({_id : videosBlockId}, {$push: {ref_pages : mainPageId}});

db.content_blocks.insert(photos);
var photosBlockId = db.content_blocks.find(photos).next()._id;
db.pages.update({_id : mainPageId}, {$push: {ref_content_blocks : photosBlockId}});
db.content_blocks.update({_id : photosBlockId}, {$push: {ref_pages : mainPageId}});

/*****************
 * CONTENT TYPES *
 *****************/
// create content_type variables
var titleFieldObjectId = new ObjectId();
var contentFieldObjectId = new ObjectId();
var imageFieldObjectId = new ObjectId();
var createdDateFieldObjectId = new ObjectId();
var article = {
  name : 'Article',
  description : 'It is an article.',
  ref_project : '',
  ref_displays : [],
  fields : [
    {
      _id : titleFieldObjectId,
      type : 'short_text',
      label : 'Title',
      description : 'This is the title of an article.'
    },
    {
      _id : contentFieldObjectId,
      type : 'long_text',
      label : 'Article content',
      description : 'This is the content of an article.'
    },
    {
      _id : imageFieldObjectId,
      type : 'image',
      label : 'Article image',
      description : 'This is an image of the given article.'
    },
    {
      _id : createdDateFieldObjectId,
      type : 'date',
      label : 'Created date',
      description : 'This is the date when created the given article.'
    }
  ]
}

var videoTitleFieldObjectId = new ObjectId();
var videoPreviewImageFieldObjectId = new ObjectId();
var video = {
  name : 'Video',
  description : 'It is a video.',
  ref_project : '',
  ref_displays : [],
  fields : [
    {
      _id : videoTitleFieldObjectId,
      type : 'short_text',
      label : 'Video Title',
      description : 'This is the title of a video.'
    },
    {
      _id : videoPreviewImageFieldObjectId,
      type : 'image',
      label : 'Preview image',
      description : 'This is the preview image of video.'
    }
  ]
}

// insert content_types to the database
db.content_types.insert(article);
var articleContentTypeId = db.content_types.find(article).next()._id;
db.projects.update({_id : indexId}, {$push: {ref_content_types : articleContentTypeId}});
db.content_types.update({_id : articleContentTypeId}, {$set : {ref_project : indexId}});

db.content_types.insert(video);
var videoContentTypeId = db.content_types.find(video).next()._id;
db.projects.update({_id : indexId}, {$push: {ref_content_types : videoContentTypeId}});
db.content_types.update({_id : videoContentTypeId}, {$set : {ref_project : indexId}});

/************
 * DISPLAYS *
 ************/
// create display variables
var article_with_title = {
  name : 'Article Title',
  description : 'It is a display mode for articles, which shows just the title of article.',
  ref_content_type : articleContentTypeId,
  fields : [
    {
      ref_field : titleFieldObjectId,
      settings : {
        label : 'false'
      }
    }
  ]
}

var article_with_title_and_with_content = {
  name : 'Article Title and Content',
  description : 'It is a display mode for articles, which shows the title and the content of article.',
  ref_content_type : articleContentTypeId,
  fields : [
    {
      ref_field : titleFieldObjectId,
      settings : {
        label : 'false'
      }
    },
    {
      ref_field : contentFieldObjectId,
      settings : {
        length : '500',
        label : 'false'
      }
    }
  ]
}

var article_with_title_and_with_content_and_with_image_and_with_date = {
  name : 'Article Title and Content',
  description : 'It is a display mode for articles, which shows the title and the content of article.',
  ref_content_type : articleContentTypeId,
  fields : [
    {
      ref_field : titleFieldObjectId,
      settings : {
        label : 'false'
      }
    },
    {
      ref_field : contentFieldObjectId,
      settings : {
        length : '500',
        label : 'false'
      }
    },
    {
      ref_field : imageFieldObjectId,
      settings : {
        size : ['50', '50'],
        label : 'true'
      }
    },
    {
      ref_field : createdDateFieldObjectId,
      settings : {
        format : 'DD-MM-YYYY',
        label : 'true'
      }
    }
  ]
}

// insert displays to the database
db.displays.insert(article_with_title);
var articleWithTitleTypeId = db.displays.find(article_with_title).next()._id;
db.content_types.update({_id : articleContentTypeId}, {$push: {ref_displays : articleWithTitleTypeId}});

db.displays.insert(article_with_title_and_with_content);
var articleWithTitleAndContentTypeId = db.displays.find(article_with_title_and_with_content).next()._id;
db.content_types.update({_id : articleContentTypeId}, {$push: {ref_displays : articleWithTitleAndContentTypeId}});

db.displays.insert(article_with_title_and_with_content_and_with_image_and_with_date);
var articleWithTitleAndContentAndImageAndDateTypeId = db.displays.find(article_with_title_and_with_content_and_with_image_and_with_date).next()._id;
db.content_types.update({_id : articleContentTypeId}, {$push: {ref_displays : articleWithTitleAndContentAndImageAndDateTypeId}});

db.content_blocks.update({_id : latestNewsBlockId}, {$push: {ref_displays : articleWithTitleTypeId}});
